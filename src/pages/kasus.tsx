import * as React from 'react';
import styled from '@emotion/styled';
import { NextPage } from 'next';
import dynamic from 'next/dynamic';

import { PageWrapper, Content, Column } from 'components/layout';
import { Box, Heading, Stack, Text, themeProps, Paragraph } from 'components/design-system';
import { DetailNumbers, SecondaryNavLink } from 'modules/kasus';
import { useRouter } from 'next/router';
import { YellowWarningIcon } from 'components/icons';

import fetchJsonP from 'fetch-jsonp';
import useSWR from 'swr/dist';

interface KasusProps {
  errors?: string;
}
const EmbeddedVisualizationNoSSR = dynamic(() => import('modules/kasus/EmbeddedVisualization'), {
  ssr: false,
});

const LastUpdated = dynamic(() => import('modules/kasus/LastUpdated'), {
  ssr: false,
});

const Section = Content.withComponent('section');
const Root = styled(Section)`
  padding: 6vh 24px;

  ${themeProps.mediaQueries.lg} {
    padding: 12vh 24px;
  }
`;

const NavGrid = styled(Box)`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  max-width: 25%;
`;

const NavContainer = styled(Box)`
  display: grid;
  grid-template-columns: 1fr 2fr;
`;

const Hero: React.FC = () => {
  return (
    <Root noPadding>
      <Column>
        <Heading variant={900} mb="md" maxWidth={800} as="h1">
          Fatality News
        </Heading>
        <Paragraph variant={500} maxWidth={800}>
          Kami merangkum data kematian COVID-19 dari berbagai portal berita dan menyajikan
          insightnya pada anda.
        </Paragraph>
      </Column>
    </Root>
  );
};

const SummaryNumberDescWrapper = styled(Box)`
  display: grid;
  grid-template-columns: 1fr 5fr;
`;

const VisualizationGrid = styled(Box)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: 276px auto;
  }
`;

const updatedAt = new Date().toISOString();
const embedTokenUrl =
  'https://kc19-fatality-news.azurewebsites.net/api/fatalitystat?code=12R0LqAqwYeGfPFpFtHfaAzJXN9eCaDoSqIwZidnPk5TzQnt2a5UWg==';

const KasusPage: NextPage<KasusProps> = () => {
  const router = useRouter();
  const [embedToken, setEmbedtoken] = React.useState('');
  const [embedUrl, setEmbedUrl] = React.useState('');
  const [reportId, setReportId] = React.useState('');
  const [activeVis, setActiveVis] = React.useState('provinsi');
  const fetch = (url: string) => {
    fetchJsonP(url, {
      jsonpCallbackFunction: 'callback',
    })
      .then(r => r.json())
      .then(r => {
        setEmbedtoken(r.EmbedToken);
        setEmbedUrl(r.EmbedUrl);
        setReportId(r.ReportId);
      });
  };
  useSWR(embedTokenUrl, fetch);
  const changeActiveVisBasedOnUrl = (url: string) => {
    const splittedUrl = url.split('=');
    if (splittedUrl.length < 2) {
      setActiveVis('provinsi');
    } else {
      const activeVisFromQueryStr = url.split('=')[1];
      setActiveVis(activeVisFromQueryStr);
    }
  };

  const onNavClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    const hrefStr = e.currentTarget.getAttribute('href');
    if (typeof hrefStr === 'string') {
      changeActiveVisBasedOnUrl(hrefStr);
    }
  };

  React.useEffect(() => {
    if (typeof router.query.activeVis === 'string') {
      setActiveVis(router.query.activeVis ?? 'provinsi');
    }
  }, [router.query.activeVis]);

  let vis;
  if (activeVis === 'provinsi') {
    vis = (
      <EmbeddedVisualizationNoSSR
        id="provinsi"
        embedToken={embedToken}
        embedUrl={embedUrl}
        reportId={reportId}
        currentPageName="provinsi"
        height="100%"
        isActive={activeVis === 'provinsi'}
      />
    );
  }
  if (activeVis === 'usia') {
    vis = (
      <EmbeddedVisualizationNoSSR
        id="usia"
        embedToken={embedToken}
        embedUrl={embedUrl}
        reportId={reportId}
        currentPageName="usia"
        height="100%"
        isActive={activeVis === 'usia'}
      />
    );
  }

  if (activeVis === 'profesi') {
    vis = (
      <EmbeddedVisualizationNoSSR
        id="profesi"
        embedToken={embedToken}
        embedUrl={embedUrl}
        reportId={reportId}
        currentPageName="profesi"
        height="100%"
        isActive={activeVis === 'profesi'}
      />
    );
  }
  const isKematianDiverifikasiActive = true;
  return (
    <PageWrapper pageTitle="Kasus">
      <Hero />
      <Section>
        <Column>
          <Stack spacing="xl" color="#B8BCC6">
            <VisualizationGrid>
              <Box>
                <Box>
                  <Box
                    display="flex"
                    flexDirection="column"
                    backgroundColor="card"
                    borderRadius={15}
                    overflow="hidden"
                  >
                    <Box height={120} overflow="hidden">
                      <EmbeddedVisualizationNoSSR
                        id="reportContainer"
                        currentPageName="kematianDiverifikasi"
                        embedToken={embedToken}
                        embedUrl={embedUrl}
                        reportId={reportId}
                        isActive={isKematianDiverifikasiActive}
                      />
                    </Box>
                    <SummaryNumberDescWrapper py="sm">
                      <Box
                        height="100px"
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Box>
                          <YellowWarningIcon width={30} height={30} />
                        </Box>
                      </Box>
                      <Box padding="5px 15px">
                        <Box>
                          <Text variant={400}>berita kematian diverifikasi</Text>
                        </Box>
                        <hr style={{ border: '0.5px solid #666B73' }} />
                        <Box>
                          <Text variant={100}>
                            Data diverifikasi oleh relawan kawalcovid19. Data ini TIDAK mewakili
                            data aktual kematian oleh COVID-19
                          </Text>
                        </Box>
                      </Box>
                    </SummaryNumberDescWrapper>
                  </Box>
                </Box>
                <br />
                <Box marginBottom={50}>
                  <Text variant={100} color="accents07" letterSpacing="0.1em">
                    RINCIAN
                  </Text>
                  <hr style={{ border: '0.5px solid #666B73' }} />
                  <DetailNumbers embedToken={embedToken} embedUrl={embedUrl} reportId={reportId} />
                </Box>
                <LastUpdated updatedAt={updatedAt} />
              </Box>
              <Box margin={0}>
                <Box>
                  <Box>
                    <Text variant={100} color="accents07" letterSpacing="0.1em">
                      LIHAT BERDASARKAN
                    </Text>
                  </Box>
                  <Box>
                    <NavContainer>
                      <NavGrid>
                        <SecondaryNavLink
                          href={{ pathname: '/kasus', query: { activeVis: 'provinsi' } }}
                          isActive={
                            !router.query.activeVis || router.query.activeVis === 'provinsi'
                          }
                          title="Daerah"
                          onClick={onNavClick}
                        />
                        <SecondaryNavLink
                          href={{ pathname: '/kasus', query: { activeVis: 'usia' } }}
                          isActive={router.query.activeVis === 'usia'}
                          title="Usia"
                          onClick={onNavClick}
                        />
                        <SecondaryNavLink
                          href={{ pathname: '/kasus', query: { activeVis: 'profesi' } }}
                          isActive={router.query.activeVis === 'profesi'}
                          title="Profesi"
                          onClick={onNavClick}
                        />
                      </NavGrid>
                    </NavContainer>
                  </Box>
                </Box>
                <br />
                <Box
                  width="100%"
                  height="100%"
                  maxHeight={500}
                  borderRadius={15}
                  overflow="hidden"
                  backgroundColor="card"
                >
                  {vis}
                </Box>
              </Box>
            </VisualizationGrid>
          </Stack>
        </Column>
      </Section>
    </PageWrapper>
  );
};

export default KasusPage;
