import * as React from 'react';
import Link, { LinkProps } from 'next/link';
import { themeGet } from '@styled-system/theme-get';
import styled from '@emotion/styled';

import { Box, Text, themeProps, TextProps } from 'components/design-system';

const NavLinkRoot = Text.withComponent('a');

interface NavLinkProps {
  title: string;
  href: LinkProps['href'];
  onClick: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  as?: LinkProps['as'];
  isActive?: boolean;
  scale?: TextProps['variant'];
}

const SecondaryNavigationBase = styled(NavLinkRoot)<Pick<NavLinkProps, 'isActive'>>`
  display: flex;
  align-items: center;
  margin-right: 24px;
  height: 36px;
  text-decoration: none;
  border-bottom-width: 2px;
  border-bottom-style: solid;
  border-bottom-color: ${props =>
    props.isActive
      ? `${themeGet('colors.brandred', themeProps.colors.brandred)(props)}`
      : 'transparent'};
  white-space: nowrap;

  &:hover,
  &:focus {
    border-bottom-color: ${themeGet('colors.brandred', themeProps.colors.brandred)};
  }

  &:last-of-type {
    margin-right: 0;
  }
`;

const SecondaryNavWrapper = styled(Box)`
  overflow-x: auto;
  overflow-y: hidden;
  white-space: nowrap;
`;

const SecondaryNavLink: React.FC<NavLinkProps> = ({
  title,
  href,
  as,
  isActive,
  scale,
  onClick,
}) => {
  return (
    <Link href={href} as={as} passHref>
      <SecondaryNavigationBase isActive={isActive} onClick={onClick}>
        <Box fontSize="24px" fontWeight="600">
          <Text variant={scale || 300}>{title}</Text>
        </Box>
      </SecondaryNavigationBase>
    </Link>
  );
};

SecondaryNavLink.defaultProps = {
  isActive: false,
};

export { SecondaryNavWrapper, SecondaryNavLink };
