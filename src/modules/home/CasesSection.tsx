import * as React from 'react';
import useSWR from 'swr/dist';
import styled from '@emotion/styled';
import format from 'date-fns/format';
import { id } from 'date-fns/locale';
import * as Sentry from '@sentry/browser';

import { Box, Heading, themeProps, Text, Stack } from 'components/design-system';
import { fetch } from 'utils/api';
import { logEventClick } from 'utils/analytics';
import SummaryApiResponse, { SUMMARY_API_URL } from 'types/api';

const EMPTY_DASH = '----';
const ERROR_TITLE = 'Error - Gagal mengambil data terbaru';
const ERROR_MESSAGE = 'Gagal mengambil data. Mohon coba lagi dalam beberapa saat.';

const Link = Text.withComponent('a');

const ReadmoreLink = styled(Link)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.sm} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.sm}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.md}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.lg}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.xl} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 4 - 48px), 1fr)
    );
  }
`;

const formatDate = (dateToFormat: Date) => {
  return format(dateToFormat, 'dd MMMM yyyy HH:mm:ss xxxxx', {
    locale: id,
  });
};

export interface CaseBoxProps {
  color: string;
  value?: number;
  label: string;
}

const CaseBox: React.FC<CaseBoxProps> = React.memo(({ color, value, label }) => (
  <Box
    display="flex"
    alignItems="center"
    justifyContent="center"
    px="md"
    pt="sm"
    pb="md"
    borderRadius={6}
    backgroundColor="card"
  >
    <Box textAlign="center">
      <Text display="block" variant={1100} color={color} fontFamily="monospace">
        {value || EMPTY_DASH}
      </Text>
      <Text display="block" mb="xxs" variant={400} color="accents08">
        {label}
      </Text>
    </Box>
  </Box>
));

export interface CasesSectionBlockProps {
  data?: SummaryApiResponse;
  error: boolean;
}

const CasesSectionBlock: React.FC<CasesSectionBlockProps> = React.memo(({ data, error }) => {
  if (error) {
    Sentry.withScope(scope => {
      scope.setTag('api_error', 'case_summary');
      Sentry.captureException(error);
    });
  }

  const SectionHeading = styled(Heading)`
    margin-bottom: ${themeProps.space.md}px;

    ${themeProps.mediaQueries.md} {
      margin-bottom: ${themeProps.space.xl}px;
    }
  `;

  const DetailsWrapper = styled(Box)`
    ${themeProps.mediaQueries.md} {
      display: flex;
      align-items: flex-end;
      justify-content: space-between;
    }
  `;

  const lastUpdatedAt = data?.metadata?.lastUpdatedAt;
  return (
    <Stack mb="xxl">
      <SectionHeading variant={800} as="h2">
        Jumlah Kasus di Indonesia Saat Ini
      </SectionHeading>
      <Box>
        <GridWrapper>
          <CaseBox color="chart" value={data?.confirmed} label="Terkonfirmasi" />
          <CaseBox color="warning02" value={data?.activeCare} label="Dalam Perawatan" />
          <CaseBox color="success02" value={data?.recovered} label="Sembuh" />
          <CaseBox color="error02" value={data?.deceased} label="Meninggal" />
        </GridWrapper>
        <DetailsWrapper>
          <Box mt="md">
            <Text as="h5" m={0} variant={200} color="accents04" fontWeight={400}>
              {error ? ERROR_TITLE : 'Pembaruan Terakhir'}
            </Text>
            <Text as="p" variant={400} color="accents07" fontFamily="monospace">
              {lastUpdatedAt ? formatDate(new Date(lastUpdatedAt)) : ERROR_MESSAGE}
            </Text>
          </Box>
          <Box mt="md">
            <ReadmoreLink
              href="http://kcov.id/statistik-harian"
              target="_blank"
              rel="noopener noreferrer"
              color="primary02"
              variant={500}
              fontWeight={600}
              onClick={() => logEventClick('Statistik Harian (cases section)')}
            >
              Lihat Statistik Harian &rarr;
            </ReadmoreLink>
          </Box>
        </DetailsWrapper>
      </Box>
    </Stack>
  );
});

const CasesSection: React.FC = () => {
  const { data, error } = useSWR<SummaryApiResponse | undefined>(SUMMARY_API_URL, fetch);
  return <CasesSectionBlock data={data} error={!!error} />;
};

export default CasesSection;
