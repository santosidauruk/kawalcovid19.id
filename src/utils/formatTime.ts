import format from 'date-fns/format';
import { id } from 'date-fns/locale';

const formatTime = (date: Date) => `${format(date, 'dd MMMM yyyy', { locale: id })}`;

export default formatTime;
