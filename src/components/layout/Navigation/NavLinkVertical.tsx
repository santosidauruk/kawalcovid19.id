import * as React from 'react';
import Link from 'next/link';
import styled from '@emotion/styled';

import { UnstyledButton, Box } from 'components/design-system';
import { logEventClick } from 'utils/analytics';
import { NavInner } from './components/NavComponents';

interface NavLinkProps {
  title: string;
  href: string;
  as?: string;
  isActive?: boolean;
  icon?: React.ReactNode;
  isDark?: boolean;
}

const LinkIcon = styled(UnstyledButton)``;
const NavLabel = styled(Box)`
  margin-left: 10px;
`;
const NavInnerVertical = styled(NavInner)`
  border-bottom: 1px solid;
  margin: 0 -24px 0 -24px;
  padding: 8px 24px 9px 24px;
`;

const NavLinkVertical: React.FC<NavLinkProps> = ({ title, href, as, icon, isDark }) => {
  return (
    <Link href={href} as={as} passHref>
      <NavInnerVertical
        display="flex"
        flexDirection="row"
        onClick={() => logEventClick(title)}
        style={{ borderColor: isDark ? '#2e343b' : '#e1e2e6' }}
      >
        <LinkIcon type="button">{icon}</LinkIcon>
        <NavLabel>{title}</NavLabel>
      </NavInnerVertical>
    </Link>
  );
};

NavLinkVertical.defaultProps = {
  isActive: false,
};

export default NavLinkVertical;
