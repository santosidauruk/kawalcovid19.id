# kawalcovid19.id

> Source code for [Kawal COVID-19 website](https://kawalcovid19.id).

## Getting Started

### Installation

Run the following command to install the required dependencies. Note that we use [Yarn](https://yarnpkg.com/), not npm.

```bash
yarn
```

### Development

```bash
# serve with hot reload at localhost:3000
yarn dev

# build for production
yarn build

# start production build locally (note: you need to run `yarn build` first)
yarn start
```

## Contributing

1. Start from the open tasks in the _rightmost_ status column of [development board](https://gitlab.com/kawalcovid19/website/kawalcovid19.id/-/boards), with the highest priority in that column.
2. If the task shows no activity in **last 3 days**, you can post a comment to ask for any updates, or _assign yourself_ if you are also interested to solve that issue.
3. Prioritize tasks in the _right_-side status column; **avoid** starting to work on tasks in the _left_-side status column when the number of tasks in the right-side column is close to the [_WIP limit_](https://www.atlassian.com/agile/kanban/wip-limits).
4. Create a merge request (MR) and **mention** the related tasks in the MR description.

---

For more details about [this project](https://gitlab.com/kawalcovid19/handbook/-/blob/master/overview.md),
contribution [guidelines](https://gitlab.com/kawalcovid19/handbook/-/blob/master/CONTRIBUTING.md),
and other useful stuffs, please refer to [this handbook](https://gitlab.com/kawalcovid19/handbook/).
